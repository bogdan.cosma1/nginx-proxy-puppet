include nginx

# execute 'apt-get update'
exec { 'apt-update':                    # exec resource named 'apt-update'
  command => '/usr/bin/apt-get update'  # command this resource will run
}

#install net-tools
package { 'net-tools':
  ensure  => installed,
}

#ensure nginx is installed and service is running
class nginx {
  package { 'nginx':
   ensure => installed,
 }
  service { 'nginx':
    ensure => running,
    require => Package['nginx'],
  }
}

#replace nginx.conf file with template to match the requirements,validate the new conf and restart nginx
file { '/etc/nginx/nginx.conf':
    content => template('/etc/puppetlabs/code/environments/production/manifests/interview_test/templates/nginx.conf.bk'),
    notify => Service["nginx"],
    validate_cmd => '/usr/sbin/nginx -t',
  }

