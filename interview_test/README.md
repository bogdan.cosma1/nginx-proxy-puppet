# interview_test

## Description

2 Files of interest (location below in usage)
: 
nginx.pp -- puppet manifest file to install nginx and copy the template.conf file
nging.conf -template file for nginx.con


## Setup

1.I created a virtual host that listens to port 443 to handle the ssl protocol,set the domain from where incoming requests will be handled.Also set two proxy passes to redirect incoming requests.
2.Created a virtual host as forward proxy with the listed port of 3333,google.com as the only whitelisted site and dns resolver as 8.8.8.8.Set the log format and location for both access and error logs,log entry should look like this
127.0.0.1 "CONNECT firefox.settings.services.mozilla.com:443 HTTP/1.1" 200 "60.070" "HTTP/1.1"
3.virtual host for health check with 8080 as port to listen without logging


## Usage

command to run : puppet apply nginx.pp located in /etc/puppetlabs/code/environment/production/modules/interview_test/manifests/
note: make sure the nginx.conf file is in "/etc/puppetlabs/code/environment/production/modules/interview_test/templates/" ,otherwise this needs to be changed in the manifest file 


To test it out you need to configure a proxy in your browser as localhost with the port 3333 and keep in mind that only google.com is whitelisted 

